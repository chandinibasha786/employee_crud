const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let employeeSchema = new Schema({
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  email: {
    type: String
  },
  address: {
    type: String
  },
  phone: {
    type: Number
  },
  department: {
    type: String
  },
  project: {
    type: String
  }
}, {
    collection: 'employee'
  })

module.exports = mongoose.model('Employee', employeeSchema)