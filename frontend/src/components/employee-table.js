import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Button from "react-bootstrap/Button";
import "../App.css";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

export default class EmployeeTable extends Component {
  constructor(props) {
    super(props);
    this.deleteEmployee = this.deleteEmployee.bind(this);
  }

  deleteEmployee() {
    axios
      .delete(
        "http://localhost:4000/employee/delete-employee/" + this.props.obj._id
      )
      .then((res) => {
        toast.success("Employee successfully deleted!");
      })
      .catch((error) => {
        toast.success(error);
      });
  }

  render() {
    return (
      <>
        <ToastContainer />
        <tr>
          <td>{this.props.obj.firstname}</td>
          <td>{this.props.obj.lastname}</td>
          <td>{this.props.obj.email}</td>
          <td>{this.props.obj.phone}</td>
          <td>{this.props.obj.address}</td>
          <td>{this.props.obj.project}</td>
          <td>{this.props.obj.department}</td>
          <td>
            <div className="edit-buttons">
              <Link
                className="edit-link"
                path={"product/:id"}
                to={"/edit-employee/" + this.props.obj._id}
              >
                <ion-icon name="pencil-outline"></ion-icon>
              </Link>

              <Button
                onClick={this.deleteEmployee}
                className="delete"
                size="sm"
                variant="danger"
              >
                <ion-icon name="trash-outline"></ion-icon>
              </Button>
            </div>
          </td>
        </tr>
      </>
    );
  }
}
