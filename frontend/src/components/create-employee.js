//importing external dependencies 
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

//importing css for styling purpose 
import "../components/create.css";

//importing param validators
import {
  _firstname_validator,
  _lastname_validator,
  _phone_validator,
  _email_validator,
  _address_validator,
  _project_validator,
  _department_validator
} from "../../src/react-validators/param_validators";


//function  to addemp using validators 

function AddEmp() {
  const history = { useHistory };

  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [project, setProject] = useState("");
  const [department, setDepartment] = useState("");
  const [headStatus, setHeadStatus] = useState("");

  const [firstnameError, setFirstNameError] = useState("");
  const [lastnameError, setLastNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const [addressError, setAddressError] = useState("");
  const [projectError, setProjectError] = useState("");
  const [departmentError, setDepartmentError] = useState("");

  // FirstName
  const onChangeFirstName = (event) => {
    setFirstName(event.target.value);
    setFirstNameError("");
  };

  const onBlurFirstName = (event) => {
    validateFirstName(event.target.value);
  };
  const validateFirstName = (firstname) => {
    setFirstNameError(_firstname_validator(firstname));
  };

    // LastName
    const onChangeLastName = (event) => {
      setLastName(event.target.value);
      setLastNameError("");
    };
  
    const onBlurLastName = (event) => {
      validateLastName(event.target.value);
    };
    const validateLastName = (lastname) => {
      setLastNameError(_lastname_validator(lastname));
    };

  
  //Email
  const onChangeEmail = (event) => {
    setEmail(event.target.value);
    setEmailError("");
  };

  const onBlurEmail = (event) => {
    validateEmail(event.target.value);
  };
  const validateEmail = (email) => {
    setEmailError(_email_validator(email));
  };

  //Phone number
  const onChangePhone = (event) => {
    setPhone(parseInt(event.target.value));
    setPhoneError("");
  };

  const onBlurPhone = (event) => {
    validatePhone(event.target.value);
  };
  const validatePhone = (phone) => {
    setPhoneError(_phone_validator(phone));
  };
 
  //address
  const onChangeAddress = (event) => {
    setAddress(event.target.value);
    setAddressError("");
  };

  const onBlurAddress = (event) => {
    validateAddress(event.target.value);
  };
  const validateAddress = (address) => {
    setAddressError(_address_validator(address));
  };

    //project
    const onChangeProject = (event) => {
      setProject(event.target.value);
      setProjectError("");
    };
  
    const onBlurProject = (event) => {
      validateProject(event.target.value);
    };
    const validateProject = (project) => {
      setProjectError(_project_validator(project));
    };

      //department
  const onChangeDepartment = (event) => {
    setDepartment(event.target.value);
    setDepartmentError("");
  };

  const onBlurDepartment = (event) => {
    validateDepartment(event.target.value);
  };
  const validateDepartment = (department) => {
    setDepartmentError(_department_validator(department));
  };

  const resetErrors = () => {
    setFirstNameError("");
    setLastNameError("");
    setEmailError("");
    setPhoneError("");
    setProjectError("");
    setDepartmentError("");
    setAddressError("")
  };

  const resetFields = () => {
    setFirstName("");
    setLastName("");
    setEmail("");
    setPhone("");
    setProject("");
    setDepartment("");
    setAddress("")
  };

  //time out function
  const clearStatus = () => {
    setHeadStatus("");
  };

  //submitting the form
  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validateFirstName(firstname);
    validateLastName(lastname);
    validateEmail(email);
    validatePhone(phone);
    validateProject(project);
    validateDepartment(department);
    validateAddress(address)

    if (
      _firstname_validator(firstname).length > 0 ||
      _lastname_validator(lastname).length > 0 ||
      _email_validator(email).length > 0 ||
      _phone_validator(phone).length > 0 ||
      _project_validator(project).length > 0 ||
      _department_validator(department).length > 0 ||
      _address_validator(address).length >0    
      ) {
      console.log("ERROR");
      return;
    } else {
      const userDetails = {
        firstname,
        lastname,
        email,
        phone,
        project,
        department,
        address,
      };
      const url = "http://localhost:4000/employee/create-employee";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          },
      };

      try {
        const response = await fetch(url, options);
        //console.log(response.status)
        const data = await response.json();
        // console.log("data :", data);
        if (data.msg === "Employee created successfully") {
          setHeadStatus(data.msg);
          setTimeout(clearStatus, 4000);
          resetFields();
        } else {
          setHeadStatus(data.msg);
          setTimeout(clearStatus, 5000);
          toast.success("Employee created successfully");
        }
      } catch (error) {
        // unknown error
        setHeadStatus("Unable to contact server.");
      }
    }
  };
  
  //returns the html page with the above validations to add a new user into DB
  return (
    <div className="container_admin">
      <div className="wrapper_admin">
        <ToastContainer/>
        <form>
          <h1>Create Employee Details</h1>
        <h1 className="headstatus" style={{color:"red", fontSize:"14px"}}>{headStatus}</h1>
        <div>First Name</div>
          <input
            type="text"
            id="firstname"
            name="firstname"
            value={firstname}
            onChange={(event) => onChangeFirstName(event)}
            onBlur={(event) => onBlurFirstName(event)}
            required
            placeholder="First Name"
          />
          <div style={{color:"red"}}>{firstnameError}</div>
          <div>Last Name</div>
          <input
            type="text"
            id="lastname"
            name="lastname"
            value={lastname}
            onChange={(event) => onChangeLastName(event)}
            onBlur={(event) => onBlurLastName(event)}
            required
            placeholder="Last Name"
          />
          <div style={{color:"red"}}>{lastnameError}</div>
          <div>Address</div>
          <input
            type="text"
            id="address"
            name="address"
            value={address}
            onChange={(event) => onChangeAddress(event)}
            onBlur={(event) => onBlurAddress(event)}
            placeholder="Address"
          />
          <div style={{color:"red"}}>{addressError}</div>
          <div>Email</div>
          <input
            type="email"
            id="email"
            name="Email"
            value={email}
            onChange={(event) => onChangeEmail(event)}
            onBlur={(event) => onBlurEmail(event)}
            required
            placeholder="Email"
          />
          <div style={{color:"red"}}>{emailError}</div>
          <div>Phone Number</div>
          <input
            type="number"
            id="phone"
            name="Phone"
            value={phone}
            onChange={(event) => onChangePhone(event)}
            onBlur={(event) => onBlurPhone(event)}
            required
            placeholder="Phone Number"
          />
          <div style={{color:"red"}}>{phoneError}</div>
          <div>Project Name</div>
          <input
            type="text"
            id="project"
            name="Project"
            value={project}
            onChange={(event) => onChangeProject(event)}
            onBlur={(event) => onBlurProject(event)}
            required
            placeholder="Project"
          />
          <div style={{color:"red"}}>{projectError}</div>
          <div>Department</div>
          <input
            type="text"
            id="department"
            name="Department"
            value={department}
            onChange={(event) => onChangeDepartment(event)}
            onBlur={(event) => onBlurDepartment(event)}
            required
            placeholder="Department"
          />
          <div style={{color:"red"}}>{departmentError}</div>
          <input
            type="submit"
            id="login-btn"
            value="Add"
            onClick={submitForm}
          />
        </form>
      </div>
    </div>
  );
}

//exporting AddEmp module
export default AddEmp;
