import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import EmployeeTable from './employee-table';


export default class EmployeeList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      employee: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:4000/employee/')
      .then(res => {
        this.setState({
          employee: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  DataTable() {
    return this.state.employee.map((res, i) => {
      return <EmployeeTable obj={res} key={i} />;
    });
  }


  render() {
    return (
    <div className="table-wrapper w-100 mt-5">
      <h1 className="text-center text-primary mb-4 mt-5">Employee List</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Project</th>
            <th>Department</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}