import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

export default class EditEmployee extends Component {
  constructor(props) {
    super(props);

    this.onChangeEmployeeFirstName = this.onChangeEmployeeFirstName.bind(this);
    this.onChangeEmployeeEmail = this.onChangeEmployeeEmail.bind(this);
    this.onChangeEmployeeLastName = this.onChangeEmployeeLastName.bind(this);
    this.onChangeEmployeePhone = this.onChangeEmployeePhone.bind(this);
    this.onChangeEmployeeAddress = this.onChangeEmployeeAddress.bind(this);
    this.onChangeEmployeeDepartment =
      this.onChangeEmployeeDepartment.bind(this);
    this.onChangeEmployeeProject = this.onChangeEmployeeProject.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      phone: "",
      address: "",
      department: "",
      project: "",
    };

    this.baseState = this.state;
  }

  resetForm = () => {
    this.setState(this.baseState);
  };

  componentDidMount() {
    axios
      .get(
        "http://localhost:4000/employee/edit-employee/" +
          this.props.match.params.id
      )
      .then((res) => {
        this.setState({
          firstname: res.data.firstname,
          lastname: res.data.lastname,
          email: res.data.email,
          phone: res.data.phone,
          address: res.data.address,
          department: res.data.department,
          project: res.data.project,
        });
        toast.success("edit employee in progress");
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onChangeEmployeeFirstName(e) {
    this.setState({ firstname: e.target.value });
  }

  onChangeEmployeeLastName(e) {
    this.setState({ lastname: e.target.value });
  }

  onChangeEmployeeEmail(e) {
    this.setState({ email: e.target.value });
  }

  onChangeEmployeePhone(e) {
    this.setState({ phone: e.target.value });
  }

  onChangeEmployeeAddress(e) {
    this.setState({ address: e.target.value });
  }

  onChangeEmployeeDepartment(e) {
    this.setState({ department: e.target.value });
  }

  onChangeEmployeeProject(e) {
    this.setState({ project: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const employeeObject = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      phone: this.state.phone,
      address: this.state.address,
      department: this.state.department,
      project: this.state.project,
    };

    axios
      .put(
        "http://localhost:4000/employee/update-employee/" +
          this.props.match.params.id,
        employeeObject
      )
      .then((res) => {
        toast.success("updated employee");
      })
      .catch((error) => {
        toast.error(error);
      });

    setTimeout(() => {
      // Redirect to employee List
      this.props.history.push("/employee-list");
    }, 2000);
  }

  render() {
    return (
      <div className="form-wrapper">
        <Form onSubmit={this.onSubmit}>
          <h1 className="text-center text-primary mb-4 mt-5">
            Edit Employee Details
          </h1>
          <ToastContainer />
          <Form.Group controlId="FirstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              className="text-capitalize"
              value={this.state.firstname}
              onChange={this.onChangeEmployeeFirstName}
            />
          </Form.Group>

          <Form.Group controlId="LastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              className="text-capitalize"
              value={this.state.lastname}
              onChange={this.onChangeEmployeeLastName}
            />
          </Form.Group>

          <Form.Group controlId="Email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              className="text-lowercase"
              value={this.state.email}
              onChange={this.onChangeEmployeeEmail}
            />
          </Form.Group>

          <Form.Group controlId="Phone">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control
              type="number"
              value={this.state.phone}
              onChange={this.onChangeEmployeePhone}
            />
          </Form.Group>

          <Form.Group controlId="Address">
            <Form.Label>Address</Form.Label>
            <Form.Control
              type="text"
              className="text-capitalize"
              value={this.state.address}
              onChange={this.onChangeEmployeeAddress}
            />
          </Form.Group>

          <Form.Group controlId="Project">
            <Form.Label>Project</Form.Label>
            <Form.Control
              type="text"
              className="text-capitalize"
              value={this.state.project}
              onChange={this.onChangeEmployeeProject}
            />
          </Form.Group>

          <Form.Group controlId="Department">
            <Form.Label>Department</Form.Label>
            <Form.Control
              type="text"
              className="text-capitalize"
              value={this.state.department}
              onChange={this.onChangeEmployeeDepartment}
            />
          </Form.Group>
          <Button
            variant="danger"
            onClick={this.resetForm}
            size="lg"
            block="block"
            type="button"
            className="mt-4 mb-4 float-start"
          >
            Clear
          </Button>

          <Button
            variant="success"
            size="lg"
            block="block"
            type="submit"
            className="mt-4 mb-4 float-end"
          >
            Update Employee
          </Button>
        </Form>
      </div>
    );
  }
}
