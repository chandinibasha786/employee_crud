/**
 * Helper function to validate firstname string
 * checks if firstname is string, else returns "firstname should be of string type"
 * checks if firstname is too short, else returns "firstname should be at least 4 characters long and max of 25"
 * checks if firstname is of the correct format, else returns "firstname should not have at least 1 special character and at least 1 digit"
 * @param {} firstname
 * @returns
 */
 const _firstname_validator = (firstname) => {

    if (typeof firstname != "string") {
       return "firstname should be of string type";
    }
  
    if (firstname.length < 3) {
      return "firstname length should be at least 4 characters long";
    }
  
    if (firstname.length > 25) {
      return "firstname length shouldn't be 25 characters long";
    }
  
    if (!firstname.match(/^[a-zA-Z ]+$/)) {
      return "firstname should be of the correct format";
    }
  
    return "";
  };

  /**
 * Helper function to validate lastname string
 * checks if lastname is string, else returns "lastname should be of string type"
 * checks if lastname is too short, else returns "lastname should be at least 4 characters long and max of 25"
 * checks if lastname is of the correct format, else returns "lastname should not have at least 1 special character and at least 1 digit"
 * @param {} lastname
 * @returns
 */
 const _lastname_validator = (lastname) => {

    if (typeof lastname != "string") {
       return "lastname should be of string type";
    }
  
    if (lastname.length < 3) {
      return "lastname length should be at least 4 characters long";
    }
  
    if (lastname.length > 25) {
      return "lastname length shouldn't be 25 characters long";
    }
  
    if (!lastname.match(/^[a-zA-Z ]+$/)) {
      return "lastname should be of the correct format";
    }
  
    return "";
  };
  
  
  /**
   * Helper function to validate email string
   * checks if email is string, else returns "email should be of string type"
   * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
   * checks if email is of the correct format, else returns "email should be of the correct format" 406
   * @param {} email
   * @returns
   */
  const _email_validator = (email) => {
    // is it a string
    if (typeof email != "string") {
      return "email should be of string type";
    }
    if (email.length < 6) {
      return "email length should be at least 6 characters long";
    }
    if (email.length > 50) {
      return "email length should be a max of 50";
    }
    if (
      !email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      return "email should be of the correct format";
    }
  
    return "";
  };
  
  /**
   * Helper function to validate phone number
   * checks if phone no is number, else returns "phone number should be of number type"
   * checks if phone no is too short, else returns "phone number should be at least 1 characters long and max of 10"
   * checks if phone no is of the correct format, else returns "phone number should be of the correct format" 406
   * @param {} phone
   * @returns
   */
  const _phone_validator = (phone) => {
    // if (typeof phoneNo != "number") {
      // return "phone number should be of number type";
    // }
  
    if (phone.toString().length < 10) {
      return "phone number length should be at least 10 characters long";
    }
  
    if (phone.length > 10) {
      return "phone number length should be a max of 10";
    }
    
    if (
        !phone.toString().match('^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,7}$')
      ) 
      {
        return "phone number should be of the correct format";
      }
      return "";
  };
  
  
  /**
   * Helper function to validate address
   * it checks wether the address is in string type or not.
   * @param {} address
   * @returns
   */
  const _address_validator = (address) => {
    if (typeof address != "string") {
      return "address should be of string type";
    }
    return "";
  };

    /**
   * Helper function to validate project
   * it checks wether the project is in string type or not.
   * @param {} project
   * @returns
   */
     const _project_validator = (project) => {
        if (typeof project != "string") {
          return "project should be of string type";
        }
        return "";
      };

  /**
   * Helper function to validate department
   * it checks wether the department is in string type or not.
   * @param {} department
   * @returns
   */
   const _department_validator = (department) => {
    if (typeof department != "string") {
      return "department should be of string type";
    }
    return "";
  };
  
  
  export {
    _firstname_validator,
    _lastname_validator,
    _phone_validator,
    _email_validator,
    _address_validator,
    _project_validator,
    _department_validator
  };
  